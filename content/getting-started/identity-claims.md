+++
title = "Verifying identity claims"
+++

Let's see how to verify identity claims.

## Obtain a public key for verification

The idea is that anyone can add identity claims of various platforms in their keys. Since this information is kept in the public key, you could take anyone's public key and check yourself whether they indeed have control over the accounts they claim to.

If you already have a public key (or its fingerprint) with OpenPGP identity claims you would like to use to verify, great! If not, you could use the following fingerprint:

```
3637202523e7c1309ab79e99ef2dc5827b445f4b
```

## Verify proofs

Open the [keyoxide.org/3637202523e7c1309ab79e99ef2dc5827b445f4b](https://keyoxide.org/3637202523e7c1309ab79e99ef2dc5827b445f4b) page.

You now see a list of domains and/or accounts on platforms for which the owner of the public key claims to have an control over. If you see a green tick next to it, the identity claim was verified: it really is their account! If you see a red cross, whoops, either they made a mistake or trying to claim an account which isn't theirs.

## Your turn

If you'd like to add decentralized OpenPGP identity claims to your key, go to the [Adding claims](/key-management/adding-claims) guide and find the right one for your platform of choice. You may find the process to be remarkably easy.

If your platform is not listed, it's not supported yet. See the [contributing guide](/keyoxide/contributing) for more information on how to get that platform supported.
