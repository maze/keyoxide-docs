+++
title = "Overview"

template = "section.html"
page_template = "page.html"
+++

Let's get you started on setting up your online identity and profile page.

If you'd like to know more about how Keyoxide works before getting started, please have a look at the [What is Keyoxide?](/getting-started/what-is-keyoxide) page.

## Creating an account

Well, you can't.

But you don't need to!

Keyoxide is not your typical "web application" requiring you to create an account and log in to perform tasks. That sounds strange but this approach has many advantages!

If you'd like to know more about the theoretical concepts, have a look at the [What is Keyoxide?](/getting-started/what-is-keyoxide) page.

## Generating an OpenPGP key

<p class="warning">Working with OpenPGP keys and identity claims requires you to work with the command line!</p>

You only need to generate an OpenPGP key once. After this step, you will add and remove claims using always the same key.

Install the **gpg** tool. It comes pre-installed with most Linux distributions.

Generate a new cryptographic key:

```bash
gpg --full-gen-key
```

Choose **RSA and RSA** by just pressing enter:

```
Please select what kind of key you want:
   (1) RSA and RSA (default)
   (2) DSA and Elgamal
   (3) DSA (sign only)
   (4) RSA (sign only)
  (14) Existing key from card
Your selection?
```

Choose **3072** by just pressing enter:

```
RSA keys may be between 1024 and 4096 bits long.
What keysize do you want? (3072)
```

Choose **2y** to make your key valid for two years (it's simple to extend this period):

```
Please specify how long the key should be valid.
         0 = key does not expire
      <n>  = key expires in n days
      <n>w = key expires in n weeks
      <n>m = key expires in n months
      <n>y = key expires in n years
Key is valid for? (0) 2y
```

Confirm this:

```
Key expires at Sun 28 Aug 2022 01:23:45 AM CEST
Is this correct? (y/N) y
```

Now, enter your name and email address (you can leave the comment empty):

```
GnuPG needs to construct a user ID to identify your key.

Real name: Alice Bobdottir
Email address: alice@bobdottir.net
Comment:
```

This email needs to be valid! You will receive an email later in this process.

Confirm this:

```
You selected this USER-ID:
    "Alice Bobdottir <alice@bobdottir.net>"

Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit? o
```

The next screen asks you to input a password to secure the key. Please use a long but memorable password and save it using a password manager: if you lose this password, you won't be able to edit the claims in the future.

Once you confirm your password, the following message appears:

```
We need to generate a lot of random bytes. It is a good idea to perform
some other action (type on the keyboard, move the mouse, utilize the
disks) during the prime generation; this gives the random number
generator a better chance to gain enough entropy.
```

Once this is done, you have generated your OpenPGP key! To find your so-called **fingerprint**, run:

```bash
gpg --fingerprint
```

One of the first lines consists of 10 blocks of 4 hexadecimal characters: this is your **fingerprint**, the identifier that is globally unique to your key. Write it down somewhere or remember how you can make it appear, you will need it later.

## Uploading your key

Your computer now has a cryptographic keypair: both the private key and the public key. Next, we upload your public key to a special server whose only purpose is to contain the public keys of many people.

### First time

Export the public key to a file named **pubkey.asc** (make sure to replace EMAIL):

```bash
gpg --armor --export EMAIL > pubkey.asc
```

Go to [keys.openpgp.org/upload](https://keys.openpgp.org/upload) and upload your key using the form.

To prove ownership of the email address you entered in the key, the server will send you an email asking you to confirm by clicking a link. Once this process is done, it does not need to be repeated and uploading becomes a lot simpler by following the instructions below.

### Subsequent times

Simply run:

```bash
gpg --keyserver hkps://keys.openpgp.org --send-keys FINGERPRINT`
```

I told you we were going to need that fingerprint! Replace the FINGERPRINT above with yours (remove all spaces!).

## Adding a claim

By the way, you now already have a Keyoxide profile! Go to the [Keyoxide.org](https://keyoxide.org) website, enter the fingerprint in the form, choose **HKP** as protocol, press the **Generate profile** button and voilà! There's the your profile page.

Let's spice up that profile page with a claim. Adding a claim always requires you to do two steps:

- telling your online account on some website about your key, and
- telling your key about that account on some website.

By performing an action on both your online account and inside your key, Keyoxide can prove for you that you have control over both the online account and the key. Go to the [Adding claims](/key-management/adding-claims) page and find one that suits the online account you wish to prove: this could be a [Twitter](/service-providers/twitter), [Mastodon](/service-providers/mastodon) or [Github](/service-providers/github) account.

<p class="warning">All claims stored inside keys start with <b>proof@ariadne.id=</b>. Never change this part! More information on the <a href="/getting-started/what-is-ariadne-id">What is ariadne.id?</a> page.</p>

Once you've uploaded your key again as per the guide's instructions, reload your profile page: it should now have your shiny new claim with a green **Verified** label next to it. Congratulations!

## HELP, AN ERROR!

Is Keyoxide giving an error when you try to view your profile in the browser? Make sure that you have uploaded your key once using the [upload form](https://keys.openpgp.org/upload) **AND** that you have clicked the link inside the confirmation email: your profile page can't be displayed before this is done.
