+++
title = "Deleting claims"
+++

Over time, you may need to delete proofs. Changing proofs can be achieved by deleting proofs and adding new ones.

## Delete all proofs in GnuPG

First, edit the key (make sure to replace FINGERPRINT):

```bash
gpg --edit-key FINGERPRINT
```

Launch the notation prompt:

```
notation
```

Enter the 'none' notation to delete all notations:

```
none
```

Save the changes:

```
save
```

## Delete one of your proofs in GnuPG

First, edit the key (make sure to replace FINGERPRINT):

```bash
gpg --edit-key FINGERPRINT
```

Launch the notation prompt:

```
notation
```

Enter the **-** (minus) symbol followed by the proof you want to delete. Make sure you type the proof exactly like it is in your key.

```
-proof@ariadne.id=dns:yourdomain.org?type=TXT
```

_To make it easier to enter the right proof, you could first [list all proofs](/key-management/listing-claims) and simply copy the proof (including "proof@ariadne.id=") you want to delete._

Save the changes:

```
save
```

Upload the key to WKD or use the following command to upload the key to [keys.openpgp.org](https://keys.openpgp.org) (make sure to replace FINGERPRINT):

```bash
gpg --keyserver hkps://keys.openpgp.org --send-keys FINGERPRINT
```
