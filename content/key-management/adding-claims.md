+++
title = "Adding claims"
+++

[dev.to](/service-providers/devto)  
[Discourse](/service-providers/discourse)  
[DNS](/service-providers/dns)  
[Gitea](/service-providers/gitea)  
[Github](/service-providers/github)  
[Gitlab](/service-providers/gitlab)  
[Hackernews](/service-providers/hackernews)  
[IRC](/service-providers/irc)  
[Lichess](/service-providers/lichess)  
[lobste.rs](/service-providers/lobsters)  
[Mastodon](/service-providers/mastodon)  
[Matrix](/service-providers/matrix)  
[Owncast](/service-providers/owncast)  
[Pixelfed](/service-providers/pixelfed)  
[Pleroma](/service-providers/pleroma)  
[Reddit](/service-providers/reddit)  
[Twitter](/service-providers/twitter)  
[XMPP](/service-providers/xmpp)  